# Week4 - 2-3 December 2021

Day | Lesson
---|---
2 December | Solution Exercise - Lesson8 (see week3/exercises)
2 December | [MNIST digit prediction](./scripts/mnist.ipynb)
2 December | [IRIS prediction applications](./scripts/iris_training_models.ipynb)
2 December | [IRIS with svm model](./scripts/iris_support_vector_machines.ipynb)
3 December | [IRIS with decision tree model](./scripts/iris_decision_tree.ipynb)
3 December | [moons with ensemble learning and random forest](./scripts/moons_ensemble_learning_and_random_forests.ipynb)
3 December | [Introduction to NLP](./slides/intro_nlp.pdf)
3 December | [Basic text preprocessing](./scripts/basic_preprocessing_in_one_place.ipynb)
3 December | [Exploring tokenization](./scripts/exporing_tokenization.ipynb)
3 December | [Web scraping Example](./scripts/example_web_scraping.ipynb)

